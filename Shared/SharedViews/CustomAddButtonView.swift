//
//  CustomAddButtonView.swift
//  Byoo
//
//  Created by Tobi Stinner on 21/08/2022.
//

import SwiftUI

struct CustomAddButtonView: View {
   
    @Binding var toggled: Bool
    let icon: Image
    
    var body: some View {
        
        Button(action: {
            self.toggled.toggle()
        }, label: {
            self.icon
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
        })
    }
}

struct CustomAddButtonView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CustomAddButtonView(
            toggled: .constant(false),
            icon: Image("RaisedFistGreen")
        )
    }
}
