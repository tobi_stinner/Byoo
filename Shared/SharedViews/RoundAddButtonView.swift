//
//  RoundAddButtonView.swift
//  Byoo (iOS)
//
//  Created by Tobi Stinner on 21/05/2022.
//

import SwiftUI

struct RoundAddButtonView: View {
    
    @Binding var toggled: Bool
    
    var body: some View {
        
        Button(action: {
            self.toggled.toggle()
        }, label: {
            Image(systemName: "plus.circle.fill")
                .resizable()
                .frame(width: 50, height: 50)
                .foregroundColor(Color("GreenLight"))
        })
    }
}

struct RoundAddButtonView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        RoundAddButtonView(toggled: .constant(false))
            .previewLayout(.sizeThatFits)
    }
}
