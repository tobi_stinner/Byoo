//
//  ResearchContentSelection.swift
//  Byoo
//
//  Created by Tobi Stinner on 06/06/2022.
//

import Foundation

enum ResearchContentSelection: Int, Identifiable, CaseIterable {
    
    var id: Int {
        self.rawValue
    }
    
    case content, sources, todos
    
    var title: String {
        
        switch self {
        case .content:
            return "Content"
        case .sources:
            return "Sources"
        case .todos:
            return "Todos"
        }
    }
}
