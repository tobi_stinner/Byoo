//
//  ResearchContentType.swift
//  Byoo
//
//  Created by Tobi Stinner on 30/05/2022.
//

import Foundation

enum ResearchContentType: Int, Identifiable, CaseIterable {
    
    var id: Int {
        self.rawValue
    }
    
    case assertion, argument, text, source, todo
    
    var title: String {
        
        switch self {
        case .assertion:
            return "Assertion"
        case .argument:
            return "Argument"
        case .text:
            return "Text"
        case .source:
            return "Source"
        case .todo:
            return "Todo"
        }
    }
}
