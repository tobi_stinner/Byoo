//
//  ResearchItemType.swift
//  Byoo
//
//  Created by Tobi Stinner on 30/04/2022.
//

import RealmSwift

enum ResearchItemType: Int, Identifiable, PersistableEnum {
    
    var id: Int {
        self.rawValue
    }
    
    case generic, book, link, topic
    
    var icon: String {
        
        switch self {
        case .generic:
            return "questionmark.circle"
        case .book:
            return "book.circle"
        case .link:
            return "link.circle"
        case .topic:
            return "folder.circle"
        }
    }
    
    var title: String {
        
        switch self {
        case .generic:
            return "Generic"
        case .book:
            return "Book"
        case .link:
            return "Link"
        case .topic:
            return "Topic"
        }
    }
}
