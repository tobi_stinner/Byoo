//
//  ResearchTextFontType.swift
//  Byoo
//
//  Created by Tobi Stinner on 25/06/2022.
//

import RealmSwift

enum ResearchTextFontType: Int, Identifiable, PersistableEnum {
    
    var id: Int {
        self.rawValue
    }
    
    case title, headline, subheadline, body
}
