//
//  ResearchTextListType.swift
//  Byoo
//
//  Created by Tobi Stinner on 25/06/2022.
//

import RealmSwift

enum ResearchTextListType: Int, Identifiable, PersistableEnum {
    
    var id: Int {
        self.rawValue
    }
    
    case none, bullet, dash
}
