//
//  ByooApp.swift
//  Shared
//
//  Created by Tobi Stinner on 13/03/2022.
//

import SwiftUI
import RealmSwift

@main
struct ByooApp: SwiftUI.App {
    
    var body: some Scene {
        
        WindowGroup {
            TabBarView()
                .environment(\.realmConfiguration, Realm.Configuration(deleteRealmIfMigrationNeeded: true))
        }
    }
}
