//
//  ResearchTextItemModel.swift
//  Byoo
//
//  Created by Tobi Stinner on 25/06/2022.
//

import RealmSwift

class ResearchTextItemModel: Object, ObjectKeyIdentifiable {
    
    @Persisted var text = ""
    @Persisted var fontType = ResearchTextFontType.body
    @Persisted var listType = ResearchTextListType.none
}
