//
//  ResearchItem.swift
//  Byoo
//
//  Created by Tobi Stinner on 15/05/2022.
//

import RealmSwift

class ResearchItemModel: Object, ObjectKeyIdentifiable {
    
    @Persisted(primaryKey: true) var _id: ObjectId
    @Persisted var title = ""
    @Persisted var type = ResearchItemType.generic
    @Persisted var assertions = List<AssertionModel>()
    @Persisted var arguments = List<ArgumentModel>()
    @Persisted var sources = List<SourceModel>()
    @Persisted var todoList = List<TodoModel>()
    @Persisted var textContent = List<ResearchTextModel>()
    @Persisted var isChecked = false
}

extension ResearchItemModel {
    
    static func mock() -> ResearchItemModel {
        
        let researchItem = ResearchItemModel()
        researchItem.title = "Sozialer Kapitalismus"
        researchItem.type = .book
        return researchItem
    }
    
    private static var areResearchItemsPopulated: Bool {
        
        do {
            let realm = try Realm()
            let decisionObjects = realm.objects(ResearchItemModel.self)
            return decisionObjects.count >= 3
        } catch {
            print("Error, couldn't read decision objects from Realm: \(error.localizedDescription)")
            return false
        }
    }
    
    static func deleteAllObjects() {
        
        do {
            let realm = try Realm()
            realm.deleteAll()
        } catch {
            print("Error, couldn't read decision objects from Realm: \(error.localizedDescription)")
        }
    }
    
    static func bootstrapDecisionsIfNecessary() {
        
        if !self.areResearchItemsPopulated {
            
            do {
                let realm = try Realm()
                
                let todo = TodoModel()
                todo.title = "Something todo"
                
                let book = ResearchItemModel()
                book.title = "Sozialer Kapitalismus"
                book.type = .book
                
                let topic = ResearchItemModel()
                topic.title = "Demokratie"
                topic.type = .topic
                
                let generic = ResearchItemModel()
                generic.title = "Dokumentation"
                
                try realm.write {
                    book.todoList.append(todo)
                    realm.add(book)
                    topic.todoList.append(todo)
                    realm.add(topic)
                    generic.todoList.append(todo)
                    realm.add(generic)
                }
            } catch {
                print("Error, couldn't read decision objects from Realm: \(error.localizedDescription)")
            }
        }
    }
}
