//
//  Todo.swift
//  Byoo
//
//  Created by Tobi Stinner on 21/05/2022.
//

import RealmSwift

class TodoModel: Object, ObjectKeyIdentifiable {
    
    @Persisted var title = ""
    @Persisted var isChecked = false
    @Persisted var todoList = List<TodoModel>()
}
