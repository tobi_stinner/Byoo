//
//  AssertionModel.swift
//  Byoo
//
//  Created by Tobi Stinner on 30/05/2022.
//

import RealmSwift

class AssertionModel: Object, ObjectKeyIdentifiable {
    
    @Persisted var title: String
    @Persisted var arguments: List<ArgumentModel>
}
