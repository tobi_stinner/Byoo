//
//  Argument.swift
//  Byoo
//
//  Created by Tobi Stinner on 30/05/2022.
//

import RealmSwift

class ArgumentModel: Object, ObjectKeyIdentifiable {
    
    @Persisted var title: String
    @Persisted var sources = List<SourceModel>()
}
