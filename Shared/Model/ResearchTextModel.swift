//
//  ResearchTextModel.swift
//  Byoo
//
//  Created by Tobi Stinner on 25/06/2022.
//

import RealmSwift
import Foundation

class ResearchTextModel: Object, ObjectKeyIdentifiable {
    
    @Persisted var title = ""
    @Persisted var content = List<ResearchTextItemModel>()
    @Persisted var textData = Data()
    
    var richText: NSAttributedString {
        
        get {
            let data = self.textData
            guard let attributedString = try? NSKeyedUnarchiver.unarchivedObject(ofClass: NSAttributedString.self, from: data) else {
                return NSAttributedString(string: "")
            }
            return  attributedString
        }
        
        set {
                        
            do {
                try? Realm().write() {
                    guard let thawedTextModel = self.thaw() else {
                        print("Unable to thaw scrum")
                        return
                    }
//                    let data = try NSKeyedArchiver.archivedData(withRootObject: newValue, requiringSecureCoding: false)
                    thawedTextModel.textData = try NSKeyedArchiver.archivedData(withRootObject: newValue, requiringSecureCoding: false)
                }
            } catch {
                print("Failed to add text item: \(error.localizedDescription)")
            }
        }
    }
}
