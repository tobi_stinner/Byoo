//
//  SourceModel.swift
//  Byoo
//
//  Created by Tobi Stinner on 30/05/2022.
//

import RealmSwift

class SourceModel: Object, ObjectKeyIdentifiable {
    
    @Persisted var title: String
    @Persisted var quote: String
    @Persisted var link: String
    @Persisted var page: String
}
