//
//  TabBarView.swift
//  Byoo
//
//  Created by Tobi Stinner on 13/04/2022.
//

import SwiftUI

struct TabBarView: View {
    
    var body: some View {
        
        TabView() {
            
            KnowledgeView().tabItem {
                Image(systemName: "graduationcap")
            }
            
            ResearchTabView().tabItem {
                Image(systemName: "books.vertical")
            }
            
            SettingsView().tabItem {
                Image(systemName: "person")
            }
        }
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
