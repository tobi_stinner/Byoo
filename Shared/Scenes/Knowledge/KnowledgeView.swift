//
//  KnowledgeView.swift
//  Byoo
//
//  Created by Tobi Stinner on 13/04/2022.
//

import SwiftUI

struct KnowledgeView: View {
    
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct KnowledgeView_Previews: PreviewProvider {
    static var previews: some View {
        KnowledgeView()
    }
}
