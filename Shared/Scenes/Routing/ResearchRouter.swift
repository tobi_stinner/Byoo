//
//  ResearchRouter.swift
//  Byoo
//
//  Created by Tobi Stinner on 20/08/2022.
//

import SwiftUI

final class ResearchRouter: ObservableObject {
    
    @Published var path = NavigationPath()
}
