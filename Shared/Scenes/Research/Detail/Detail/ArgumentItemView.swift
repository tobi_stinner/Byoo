//
//  ArgumentItemView.swift
//  Byoo
//
//  Created by Tobi Stinner on 12/06/2022.
//

import SwiftUI

struct ArgumentItemView: View {
    
    let argumentModel: ArgumentModel
    @State var showSources = false
    let didSelectSource: ((SourceModel) -> Void)?
    
    var body: some View {
        
        VStack {
            HStack {
                Image("RaisedFistGreen")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 25, height: 25)
                Text(self.argumentModel.title)
                Spacer()
                Button {
                    self.showSources.toggle()
                } label: {
                    self.showSources ? Image(systemName: "chevron.up") : Image(systemName: "chevron.down")
                }
            }
            if self.showSources {
                ForEach(self.argumentModel.sources) { source in
                    NavigationLink(destination: ResearchSourceView(sourceModel: source)) {
                        HStack {
                            Image(systemName: "quote.bubble")
                            Text(source.quote)
                            Spacer()
                            Image(systemName: "chevron.right")
                        }
                        .padding([.top, .leading], 5)
                    }
                }
            }
        }
    }
}

struct ArgumentItemView_Previews: PreviewProvider {
    
    static var previews: some View {
        ArgumentItemView(argumentModel: ResearchModelMock.getArgument1(), didSelectSource: nil)
            .previewLayout(.sizeThatFits)
    }
}
