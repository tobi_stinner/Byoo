//
//  ResearchArgumentView.swift
//  Byoo
//
//  Created by Tobi Stinner on 12/06/2022.
//

import SwiftUI
import RealmSwift

struct ResearchArgumentView: View {
    
    @ObservedRealmObject
    var argumentModel: ArgumentModel
    
    var body: some View {
        
        ScrollView {
            LazyVStack {
                Text(self.argumentModel.title)
                ForEach(self.argumentModel.sources) { source in
                    NavigationLink(destination: ResearchSourceView(sourceModel: source)) {
                        HStack {
                            Image(systemName: "quote.bubble")
                            Text(source.title)
                            Spacer()
                            Image(systemName: "chevron.right")
                        }
                    }
                }
            }
        }
    }
}

struct ResearchArgumentView_Previews: PreviewProvider {
    static var previews: some View {
        ResearchArgumentView(argumentModel: ResearchModelMock.getArgument1())
    }
}
