//
//  ResearchDetailItemOverview.swift
//  Byoo
//
//  Created by Tobi Stinner on 06/06/2022.
//

import SwiftUI
import RealmSwift

struct ResearchAssertionView: View {
    
    @ObservedRealmObject
    var assertionModel: AssertionModel
    @State private var isAddItemPresented = false
    
    var body: some View {
        
        NavigationView {
            ZStack {
                Color("Background")
                self.content
                self.addButton
            }
        }
    }
    
    var content: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(self.assertionModel.arguments) { argument in
                    ArgumentItemView(argumentModel: argument) { sourceModel in
                    }
                }
            }
        }
    }
    
    var addButton: some View {
        
        HStack {
            Spacer()
            VStack {
                Spacer()
                RoundAddButtonView(toggled: $isAddItemPresented)
                    .sheet(isPresented: self.$isAddItemPresented) {
                        
                    }
            }
            .padding(.bottom, 16)
        }
        .padding(.trailing, 16)
    }
}

struct ResearchDetailItemOverview_Previews: PreviewProvider {
    static var previews: some View {
        ResearchAssertionView(assertionModel: ResearchModelMock.getAssertion1())
    }
}
