//
//  ResearchLinkView.swift
//  Byoo
//
//  Created by Tobi Stinner on 12/06/2022.
//

import SwiftUI
import RealmSwift

struct ResearchSourceView: View {
    
    @StateRealmObject
    var sourceModel: SourceModel
    
    var body: some View {
        
        VStack {
            Text(self.sourceModel.title)
            Text(self.sourceModel.quote)
        }
    }
}

struct ResearchLinkView_Previews: PreviewProvider {
    static var previews: some View {
        ResearchSourceView(sourceModel: ResearchModelMock.getSource1())
    }
}
