//
//  ResearchTextView.swift
//  Byoo
//
//  Created by Tobi Stinner on 25/06/2022.
//

import SwiftUI
import RealmSwift
import RichTextKit

struct ResearchTextView: View {
    
    @StateRealmObject
    var textModel: ResearchTextModel
    @State
    var richText: NSAttributedString

    @StateObject
    var context = RichTextContext()
    @State
    private var showTransformButton = false
    
    init(
        textModel: ResearchTextModel
    ) {
        self.textModel = textModel
        self.richText = textModel.richText
        print("self.textModel.richText: \(self.textModel.richText)")
        print("self.richText: \(self.richText)")
    }
    
    var body: some View {
        
        ZStack {
            RichTextEditor(text: self.$richText, context: self.context)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .keyboardToolbar {
                    self.toolbar
                        .frame(maxWidth: .infinity, maxHeight: 40)
                        .background(.white)
                }
                .toolbar(.hidden, for: .tabBar)
                .background(Color("Background"))
                .navigationTitle(self.textModel.title)
                .onAppear {
                    print("self.richText: \(self.richText)")
                }
                .onDisappear {
                    self.textModel.richText = self.richText
            }
        }
    }
    
    private var toolbar: some View {
        
        HStack(spacing: 20) {
            Button {
                context.isBold = !context.isBold
            } label: {
                Text("B")
                    .bold(context.isBold)
            }
            .padding(.leading, 10)
            Button {
                context.isItalic = !context.isItalic
            } label: {
                Text("I")
                    .italic(context.isItalic)
            }
            Button {
                context.isUnderlined = !context.isUnderlined
            } label: {
                Text("U")
                    .underline(context.isUnderlined)
            }
            Button {
                context.fontSize = context.fontSize == 32 ? 24 : 32
                context.isBold = !context.isBold
            } label: {
                Text("Title")
                    .font(.system(size: 32, weight: .bold))
            }
            Button {
                context.fontSize = context.fontSize == 26 ? 24 : 26
                context.isBold = !context.isBold
                
            } label: {
                Text("Subtitle")
                    .font(.system(size: 26, weight: .bold))
            }
            Button {
                
            } label: {
                Image(systemName: "list.bullet")
            }
            Spacer()
        }
    }
}

struct ResearchTextView_Previews: PreviewProvider {
    static var previews: some View {
        ResearchTextView(textModel: ResearchModelMock.getTextModel())
        
    }
}
