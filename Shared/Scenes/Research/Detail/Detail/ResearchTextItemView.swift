//
//  ResearchTextItemView.swift
//  Byoo
//
//  Created by Tobi Stinner on 26/06/2022.
//

import SwiftUI
import RealmSwift

struct ResearchTextItemView: View {
    
    @StateRealmObject
    var textContent: ResearchTextItemModel
    let isNewContent: Bool
    @FocusState
    private var focus: Bool
    var addNewContent: ((ResearchTextItemModel) -> Void)?
    @State
    private var currentText: String
    
    init(
        textContent: ResearchTextItemModel,
        isNewContent: Bool,
        addNewContent: ((ResearchTextItemModel) -> Void)? = nil
    ) {
        
        self.textContent = textContent
        self.currentText = textContent.text
        self.isNewContent = isNewContent
        self.addNewContent = addNewContent
    }
    
    var body: some View {
        
        HStack(alignment: .top, spacing: 0) {
            Text(self.getListCharacter())
                .padding(.top, 7)
                .padding(.leading, self.getLeadingPadding())
                .padding(.bottom, self.getBottomPadding())
            TextEditor(text: self.self.$currentText)
                .font(self.getFontType())
                .focused(self.$focus)
                .toolbar {
                    ToolbarItemGroup(placement: .keyboard) {
                        Spacer()
                        Button {
                            self.save()
                            self.focus = false
                        } label: {
                            Text("Done")
                        }
                    }
                }
                .if(self.isNewContent) { view in
                    view.frame(height: 100)
                }
        }
    }
    
    private func save() {
        
        if self.isNewContent {
            self.addNewContent?(self.textContent)
        } else {
            self.textContent.text = self.currentText
        }
    }
    
    private func getListCharacter() -> String {
        
        switch self.textContent.listType {
        case .none:
            return ""
        case .bullet:
            return "• "
        case .dash:
            return "- "
        }
    }
    
    private func getFontType() -> Font {
        
        switch self.textContent.fontType {
        case .title:
            return .title
        case .headline:
            return .headline
        case .subheadline:
            return .subheadline
        case .body:
            return .body
        }
    }
    
    private func getBottomPadding() -> CGFloat {
        
        switch self.textContent.fontType {
        case .title:
            return 15
        case .headline:
            return 10
        case .subheadline:
            return 5
        case .body:
            return 0
        }
    }
    
    private func getLeadingPadding() -> CGFloat {
        
        switch self.textContent.listType {
        case .none:
            return 10
        case .bullet, .dash:
            return 20
        }
    }
}

struct ResearchTextItemView_Previews: PreviewProvider {
    static var previews: some View {
        ResearchTextItemView(
            textContent: ResearchModelMock.getTextItemListModel(),
            isNewContent: false
        )
    }
}
