//
//  AddAssertionView.swift
//  Byoo
//
//  Created by Tobi Stinner on 29/05/2022.
//

import SwiftUI
import RealmSwift

struct AddResearchContentView: View {
        
    @ObservedRealmObject var researchItem: ResearchItemModel
    @Binding var isAddItemPresented: Bool
//    let didAddItem: ((Object) -> Void)?

    @State private var contentType: ResearchContentType = .assertion
    @State private var newTodo = ""
    @State private var todoList = [TodoModel]()
    @StateRealmObject
    private var assertionModel = AssertionModel()
    @StateRealmObject
    private var argumentModel = ArgumentModel()
    @StateRealmObject
    private var sourceModel = SourceModel()
    @StateRealmObject
    private var textModel = ResearchTextModel()
    
    var body: some View {
        
            VStack {
                HStack {
                    Spacer()
                    Button {
                        self.isAddItemPresented = false
                    } label: {
                        Image(systemName: "xmark.circle")
                            .resizable()
                            .frame(width: 30, height: 30)
                    }
                }
                .padding()
                Text("Add Content to ") + Text(self.researchItem.title).foregroundColor(.accentColor)
                
                List {
                    
                    Section {
                        Picker(selection: self.$contentType, label: Text("Type")) {
                            ForEach(ResearchContentType.allCases) { item in
                                Text(item.title).tag(item)
                            }
                        }
                        .pickerStyle(.menu)
                    } header: {
                        Text("Content Type")
                    }
                    
                    switch contentType {
                    case .todo:
                        self.todo
                    case .assertion:
                        self.assertion
                    case .argument:
                        self.argument
                    case .source:
                        self.source
                    case .text:
                        self.text
                    }
                    
                    self.addButton
                }
            }
    }
    
    var todo: some View {
        
        Section {
            ForEach(self.todoList, id: \.self) { todo in
                Text(todo.title)
            }
            HStack {
                TextField("New Todo", text: self.$newTodo)
                Button {
                    withAnimation {
                        let todo = TodoModel()
                        todo.title = self.newTodo
                        self.todoList.append(todo)
                        self.newTodo = ""
                    }
                } label: {
                    Image(systemName: "plus.circle.fill")
                }
                .disabled(self.newTodo.isEmpty)
            }
        } header: {
            Text("Todos")
        }
    }
    
    var assertion: some View {
        
        Section {
            TextField("Title", text: self.$assertionModel.title)
        } header: {
            Text("Assertion")
        }
    }
    
    var argument: some View {
        
        Section {
            TextField("Title", text: self.$argumentModel.title)
        } header: {
            Text("Argument")
        }
    }
    
    var source: some View {
        
        Section {
            TextField("Title", text: self.$sourceModel.title)
            TextField("Quote", text: self.$sourceModel.quote)
            TextField("Link", text: self.$sourceModel.link)
            TextField("Page", text: self.$sourceModel.page)
        } header: {
            Text("Source")
        }
    }
    
    var text: some View {
        
        Section {
            TextField("Title", text: self.$textModel.title)
        } header: {
            Text("Text")
        }

    }
    
    var addButton: some View {
        
        Button {
            do {
                try Realm().write() {
                    guard let thawedResearchItem = self.researchItem.thaw() else {
                        print("Unable to thaw scrum")
                        return
                    }
                    switch contentType {
                    case .assertion:
                        thawedResearchItem.assertions.append(self.assertionModel)
                    case .argument:
                        thawedResearchItem.arguments.append(self.argumentModel)
                    case .source:
                        thawedResearchItem.sources.append(self.sourceModel)
                    case .todo:
                        thawedResearchItem.todoList.append(objectsIn: self.todoList)
                    case .text:
                        thawedResearchItem.textContent.append(self.textModel)
                    }
                    self.isAddItemPresented = false
                }
            } catch {
                print("Failed to add meeting to scrum: \(error.localizedDescription)")
            }
        } label: {
            Text("Add")
                .frame(maxWidth: .infinity, alignment: .center)
        }
    }
}

struct AddAssertionView_Previews: PreviewProvider {
    static var previews: some View {
        AddResearchContentView(
            researchItem: ResearchItemModel.mock(),
            isAddItemPresented: .constant(true))
    }
}
