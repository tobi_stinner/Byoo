//
//  ResearchDetailOverViewModel.swift
//  Byoo
//
//  Created by Tobi Stinner on 11/06/2022.
//

import SwiftUI
import RealmSwift

struct ResearchDetailOverViewModel: Identifiable {
    
    enum ResearchDetailOverViewModelType {
        case assertion(model: AssertionModel)
        case argument(model: ArgumentModel)
        case text(model: ResearchTextModel)
    }
    
    var id = UUID()
    let icon: Image
    let title: String
    let type: ResearchDetailOverViewModelType
}
