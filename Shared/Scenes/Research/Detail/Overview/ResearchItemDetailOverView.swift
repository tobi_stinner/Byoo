//
//  ResearchDetailView.swift
//  Byoo
//
//  Created by Tobi Stinner on 28/05/2022.
//

import SwiftUI
import RealmSwift

struct ResearchItemDetailOverView: View {
    
    @State private var isAddItemPresented = false
    @StateRealmObject var researchItem: ResearchItemModel
    @State private var selectedContent: ResearchContentSelection = .content
    
    var body: some View {
        
        ZStack {
            Color("Background")
            ScrollView {
                VStack {
                    Picker(selection: self.$selectedContent, label: EmptyView()) {
                        ForEach(ResearchContentSelection.allCases) { item in
                            Text(item.title).tag(item)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .id(self.selectedContent)
                    .padding()
                    
                    switch self.selectedContent {
                    case .content:
                        self.content
                    case .sources:
                        self.sources
                    case .todos:
                        self.todos
                    }
                    Spacer()
                }
            }
            self.addButton
        }
        .navigationTitle(self.researchItem.title)
        .navigationBarColor(
            backgroundColor: UIColor(named: "Background"),
            titleColor: UIColor(named: "AccentColor"),
            font: nil)
    }
    
    var addButton: some View {
        
        HStack {
            Spacer()
            VStack {
                Spacer()
                RoundAddButtonView(toggled: $isAddItemPresented)
                    .sheet(isPresented: self.$isAddItemPresented) {
                        AddResearchContentView(
                            researchItem: self.researchItem,
                            isAddItemPresented: self.$isAddItemPresented
                        )
                    }
            }
            .padding(.bottom, 16)
        }
        .padding(.trailing, 16)
    }
    
    var content: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(self.getResearchDetailOverViewModels()) { item in
                    NavigationLink(destination: self.getDestination(for: item)) {
                        ResearchDetailItemView(
                            icon: item.icon,
                            title: item.title
                        )
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                    }
                }
            }
            .padding([.top, .leading, .trailing], 16)
        }
    }
    
    var sources: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(self.researchItem.sources) { source in
                    NavigationLink(destination: ResearchSourceView(sourceModel: source)) {
                        ResearchDetailItemView(
                            icon: Image(systemName: "quote.bubble"),
                            title: source.title
                        )
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                    }
                }
            }
            .padding([.top, .leading, .trailing], 16)
        }
    }
    
    var todos: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(self.researchItem.todoList) { todo in
                    TodoView(todo: todo)
                        .padding([.top, .bottom], 4)
                        .padding([.leading, .trailing], 16)
                }
            }
            .padding([.top, .leading, .trailing], 16)
        }
    }
    
    private func getResearchDetailOverViewModels() -> [ResearchDetailOverViewModel] {
        
        var viewModels = [ResearchDetailOverViewModel]()
        
        for argument in self.researchItem.arguments {
            let viewModel = ResearchDetailOverViewModel(
                icon: Image("RaisedFistGreen"),
                title: argument.title,
                type: .argument(model: argument))
            viewModels.append(viewModel)
        }
        
        for assertion in self.researchItem.assertions {
            let viewModel = ResearchDetailOverViewModel(
                icon: Image(systemName: "hand.point.up"),
                title: assertion.title,
                type: .assertion(model: assertion))
            viewModels.append(viewModel)
        }
        
        for textContent in self.researchItem.textContent {
            let viewModel = ResearchDetailOverViewModel(
                icon: Image(systemName: "text.alignleft"),
                title: textContent.title,
                type: .text(model: textContent)
            )
            viewModels.append(viewModel)
        }
        
        return viewModels
    }
    
    @ViewBuilder
    private func getDestination(for viewModel: ResearchDetailOverViewModel) -> some View {
        
        switch viewModel.type {
        case .assertion(let model):
            ResearchAssertionView(assertionModel: model)
        case .argument(let model):
            ResearchArgumentView(argumentModel: model)
        case .text(let model):
            ResearchTextView(textModel: model)
        }
    }
}

struct ResearchDetailView_Previews: PreviewProvider {
    
    static var previews: some View {
        NavigationView {
            ResearchItemDetailOverView(researchItem: ResearchModelMock.getResearchItem())
        }
    }
}
