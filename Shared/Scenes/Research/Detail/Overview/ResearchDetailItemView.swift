//
//  ResearchDetailItemView.swift
//  Byoo
//
//  Created by Tobi Stinner on 06/06/2022.
//

import SwiftUI

struct ResearchDetailItemView: View {
    
    let icon: Image
    let title: String
    
    var body: some View {
        
        HStack {
            self.icon
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 25, height: 25)
            Text(self.title)
            Spacer()
        }
    }
}

struct ResearchDetailItemView_Previews: PreviewProvider {
    static var previews: some View {
        ResearchDetailItemView(
            icon: Image(systemName: "book.circle"),
            title: "Sozialer Kapitalismus")
        .previewLayout(.sizeThatFits)
    }
}
