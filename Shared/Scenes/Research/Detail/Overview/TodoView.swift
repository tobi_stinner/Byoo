//
//  TodoView.swift
//  Byoo
//
//  Created by Tobi Stinner on 06/06/2022.
//

import SwiftUI
import RealmSwift

struct TodoView: View {
    
    @ObservedRealmObject var todo: TodoModel
    
    var body: some View {
        
        HStack {
            Button {
                do {
                    try Realm().write() {
                        guard let thawedTodoItem = self.todo.thaw() else {
                            print("Unable to thaw scrum")
                            return
                        }
                        thawedTodoItem.isChecked.toggle()
                    }
                } catch {
                    print("Failed to add meeting to scrum: \(error.localizedDescription)")
                }
            } label: {
                Image(systemName: self.todo.isChecked ? "checkmark.square" : "square")
            }
            Text(self.todo.title)
            Spacer()
        }
    }
}

struct TodoView_Previews: PreviewProvider {
    static var previews: some View {
        TodoView(todo: ResearchModelMock.getCheckedTodo())
            .previewLayout(.sizeThatFits)
    }
}
