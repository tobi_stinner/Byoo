//
//  AddResearchItemView.swift
//  Byoo (iOS)
//
//  Created by Tobi Stinner on 21/05/2022.
//

import SwiftUI

struct AddResearchItemView: View {
    
    @Binding var isAddItemPresented: Bool
    let actionHandler: ((ResearchItemModel) -> Void)?
    
    @State private var researchItem = ResearchItemModel()
    @State private var title = ""
    @State private var type: ResearchItemType = .generic
    @State private var newTodo = ""
    
    var body: some View {
        
        NavigationView {
            
            VStack {
                
                HStack {
                    Spacer()
                    Button {
                        self.isAddItemPresented = false
                    } label: {
                        Image(systemName: "xmark.circle")
                            .resizable()
                            .frame(width: 30, height: 30)
                    }
                }
                .padding()
                Text("What would you like to research?").foregroundColor(.accentColor)
                
                List {
                    
                    Section {
                        TextField("Title", text: self.$title)
                        Picker(selection: self.$type, label: Text("Type")) {
                            ForEach(ResearchItemType.allCases) { item in
                                Text(item.title).tag(item)
                            }
                        }
                        .pickerStyle(.menu)
                    } header: {
                        Text("Research item info")
                    }
                    
                    Section {
                        ForEach(self.researchItem.todoList, id: \.self) { todo in
                            Text(todo.title)
                        }
                        HStack {
                            TextField("New Todo", text: self.$newTodo)
                            Button {
                                withAnimation {
                                    let todo = TodoModel()
                                    todo.title = self.newTodo
                                    self.researchItem.todoList.append(todo)
                                    self.newTodo = ""
                                }
                            } label: {
                                Image(systemName: "plus.circle.fill")
                            }
                            .disabled(self.newTodo.isEmpty)
                        }
                    } header: {
                        Text("Todos")
                    }
                    
                    Button {
                        self.researchItem.title = self.title
                        self.researchItem.type = self.type
                        self.actionHandler?(researchItem)
                    } label: {
                        Text("Create \(self.type.title)").frame(maxWidth: .infinity, alignment: .center)
                    }
                }
            }
        }
    }
}

struct AddResearchItemView_Previews: PreviewProvider {
    static var previews: some View {
        AddResearchItemView(isAddItemPresented: .constant(true), actionHandler: nil)
    }
}
