//
//  ResearchTabView.swift
//  Byoo
//
//  Created by Tobi Stinner on 13/04/2022.
//

import SwiftUI
import RealmSwift

struct ResearchTabView: View {
    
    @State
    private var isAddItemPresented = false
    @State
    private var isInEditMode = false
    @State
    private var router = ResearchRouter()
    @ObservedResults(ResearchItemModel.self) var researchItems
    
    var body: some View {
        
        NavigationStack(path: self.$router.path) {

            ZStack {
                Color("Background")
                VStack {
                    if self.isInEditMode {
                        self.editButtons
                    }
                    self.items
                    Spacer()
                }
                self.addButton
            }
            .navigationTitle(Text("Research"))
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Button {
                        self.isInEditMode.toggle()
                    } label: {
                        Text(self.isInEditMode ? "Finish" : "Edit")
                    }
                }
            }
        }
        .navigationBarColor(backgroundColor: UIColor(named: "Background"), titleColor: UIColor(named: "AccentColor"), font: nil)
    }
    
    var editButtons: some View {
        
        HStack(spacing: 20) {
            Button {
                for item in self.researchItems {
                    if item.isChecked {
                        self.$researchItems.remove(item)
                    }
                }
            } label: {
                HStack {
                    Image(systemName: "trash.circle")
                    Text("Delete")
                }
            }
            .frame(maxWidth: .infinity)
            .padding([.top, .bottom], 8)
            .background(.white)
            .cornerRadius(10)
            Button {
                print("Summarise Items")
            } label: {
                HStack {
                    Image(systemName: "folder.circle")
                    Text("Summarise")
                }
            }
            .frame(maxWidth: .infinity)
            .padding([.top, .bottom], 8)
            .background(.white)
            .cornerRadius(10)
        }
        .padding([.trailing, .leading], 16)
    }
    
    var items: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(self.researchItems) { item in
                    NavigationLink(value: item) {
                        ResearchItemView(
                            researchItem: item,
                            isInEditMode: self.isInEditMode
                        )
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                    }
                }
                .navigationDestination(for: ResearchItemModel.self) { item in
                    ResearchItemDetailOverView(researchItem: item)
                }
                .environmentObject(self.router)
            }
            .padding([.top, .leading, .trailing], 16)
        }
    }
    
    var addButton: some View {
        
        HStack {
            Spacer()
            VStack {
                Spacer()
                RoundAddButtonView(toggled: $isAddItemPresented)
                    .sheet(isPresented: self.$isAddItemPresented) {
                        AddResearchItemView(isAddItemPresented: self.$isAddItemPresented) { researchItem in
                            self.$researchItems.append(researchItem)
                            self.isAddItemPresented = false
                        }
                    }
            }
            .padding(.bottom, 16)
        }
        .padding(.trailing, 16)
    }
}

struct ResearchView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ResearchItemModel.bootstrapDecisionsIfNecessary()
        return ResearchTabView()
    }
}
