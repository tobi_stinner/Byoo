//
//  ReseachItemView.swift
//  Byoo
//
//  Created by Tobi Stinner on 30/04/2022.
//

import SwiftUI
import RealmSwift

struct ResearchItemView: View {
    
    @ObservedRealmObject var researchItem: ResearchItemModel
    let isInEditMode: Bool
    @State var isChecked = false
    
    var body: some View {
        
        HStack {
            if isInEditMode {
                Button {
                    do {
                        try Realm().write() {
                            guard let thawedResearchItem = self.researchItem.thaw() else {
                                print("Unable to thaw scrum")
                                return
                            }
                            thawedResearchItem.isChecked.toggle()
                        }
                    } catch {
                        print("Failed to add meeting to scrum: \(error.localizedDescription)")
                    }
                } label: {
                    Image(systemName: self.researchItem.isChecked ? "checkmark.square" : "square")
                }
                Divider().frame(width: 2, height: 50)
            }
            VStack(alignment: .leading) {
                HStack {
                    Image(systemName: self.researchItem.type.icon)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 25, height: 25)
                        .foregroundColor(.accentColor)
                    Text(self.researchItem.title)
                    Spacer()
                }
                HStack {
                    HStack {
                        Image(systemName: "hand.point.up")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25)
                            .foregroundColor(Color( "GreenLight"))
                        Text("\(self.researchItem.assertions.count)")
                    }
                    
                    Divider().frame(width: 2, height: 25)
                    
                    HStack {
                        Image("RaisedFistGreen")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 40, height: 40)
                        Text("\(self.researchItem.arguments.count)")
                    }
                    
                    Divider().frame(width: 2, height: 25)
                    
                    HStack {
                        Image(systemName: "checkmark.square")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25)
                            .foregroundColor(.blue)
                        Text("\(self.researchItem.todoList.count)")
                    }
                    
                    Divider().frame(width: 2, height: 25)
                    
                    HStack {
                        Image(systemName: "quote.bubble")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25)
                            .foregroundColor(.blue)
                        Text("\(self.researchItem.sources.count)")
                    }
                    
                    Divider().frame(width: 2, height: 25)
                    
                    HStack {
                        Image(systemName: "text.alignleft")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25)
                            .foregroundColor(.blue)
                        Text("\(self.researchItem.textContent.count)")
                    }
                }
            }
        }
    }
}

struct ReseachItemView_Previews: PreviewProvider {
    static var previews: some View {
        ResearchItemView(
            researchItem: ResearchItemModel.mock(),
            isInEditMode: false
        )
        .previewLayout(.sizeThatFits)
        .preferredColorScheme(.light)
    }
}
