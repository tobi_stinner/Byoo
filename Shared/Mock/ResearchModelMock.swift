//
//  ResearchModelMock.swift
//  Byoo
//
//  Created by Tobi Stinner on 06/06/2022.
//

import RealmSwift

class ResearchModelMock {
    
    static func getCheckedTodo() -> TodoModel {
        
        let todo = TodoModel()
        todo.title = "Checked Todo"
        todo.isChecked = true
        return todo
    }
    
    static func getUncheckedTodo() -> TodoModel {
        
        let todo = TodoModel()
        todo.title = "Unchecked Todo"
        todo.isChecked = false
        return todo
    }
    
    static func getAssertion1() -> AssertionModel {
        
        let assertion = AssertionModel()
        assertion.title = "Assertion 1"
        return assertion
    }
    
    static func getAssertion2() -> AssertionModel {
        
        let assertion = AssertionModel()
        assertion.title = "Assertion 2"
        return assertion
    }
    
    static func getArgument1() -> ArgumentModel {
        
        let argument = ArgumentModel()
        argument.title = "Argument 1"
        argument.sources.append(objectsIn: [self.getSource1(), self.getSource2()])
        return argument
    }
    
    static func getSource1() -> SourceModel {
        
        let source = SourceModel()
        source.quote = "This is a quote for a source"
        return source
    }
    
    static func getSource2() -> SourceModel {
        
        let source = SourceModel()
        source.quote = "This is another quote for a source"
        return source
    }
    
    static func getArgument2() -> ArgumentModel {
        
        let argument = ArgumentModel()
        argument.title = "Argument 2"
        return argument
    }
    
    static func getTextModel() -> ResearchTextModel {
        
        let titleItem = self.getTextTitleModel()
        
        let textHeadlineItem = ResearchTextItemModel()
        textHeadlineItem.text = "This is a headline"
        textHeadlineItem.fontType = .headline
        
        let textSubheadlineItem = ResearchTextItemModel()
        textSubheadlineItem.text = "This is a subheadline"
        textSubheadlineItem.fontType = .subheadline
        
        let textItem = self.getTextItemModel()
        
        let textListItem = self.getTextItemListModel()
        
        let textModel = ResearchTextModel()
        textModel.title = "Custom Text Content"
        textModel.content.append(objectsIn: [titleItem, textHeadlineItem, textSubheadlineItem, textItem, textListItem])
        return textModel
    }
    
    static func getTextTitleModel() -> ResearchTextItemModel {
        
        let textItem = ResearchTextItemModel()
        textItem.text = "This is a title"
        textItem.fontType = .title
        return textItem
    }
    
    static func getTextItemModel() -> ResearchTextItemModel {
        
        let textItem = ResearchTextItemModel()
        textItem.text = "This is a text"
        textItem.fontType = .body
        return textItem
    }
    
    static func getTextItemListModel() -> ResearchTextItemModel {
        
        let textItem = ResearchTextItemModel()
        textItem.text = "This is a bullet list"
        textItem.fontType = .body
        textItem.listType = .bullet
        return textItem
    }
    
    static func getResearchItem() -> ResearchItemModel {
        
        let researchItem = ResearchItemModel()
        researchItem.title = "Research Item"
        researchItem.type = .book
        let todo1 = self.getCheckedTodo()
        let todo2 = self.getUncheckedTodo()
        researchItem.todoList.append(objectsIn: [todo1, todo2])
        researchItem.assertions.append(objectsIn: [self.getAssertion1(), self.getAssertion2()])
        researchItem.arguments.append(objectsIn: [self.getArgument1(), self.getArgument2()])
        researchItem.textContent.append(self.getTextModel())
        return researchItem
    }
}
